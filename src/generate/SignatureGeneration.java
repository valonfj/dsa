package generate;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import static generate.Parameters.*;

public class SignatureGeneration {

	public static void main(String[] args) {
		try{
			KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance(SIGNING_AND_VERIFYING_ALGORITHM, SIGNING_AND_VERIFYING_PROVIDER);		
			SecureRandom random = SecureRandom.getInstance(SECURE_RANDOM_ALGORITHM, SIGNING_AND_VERIFYING_PROVIDER);
			keyGenerator.initialize(1024, random);
			
			//Generate public and private keys
			KeyPair pair = keyGenerator.generateKeyPair();
			PrivateKey privateKey = pair.getPrivate();
			PublicKey publicKey = pair.getPublic();
			
			//Instantiate the signature object
			Signature signingSignature = Signature.getInstance(SIGNATURE_ALGORITHM, SIGNING_AND_VERIFYING_PROVIDER); 
			signingSignature.initSign(privateKey);
			
			//Open the file with the text to be signed.
			FileInputStream fileInputStream = new FileInputStream(DATA_FILE_PATH);
			BufferedInputStream fileBufferInputStream = new BufferedInputStream(fileInputStream);
			byte[] fileBuffer = new byte[1024];
			int fileBufferLength;
			//Read and sign the text file(message)
			while ((fileBufferLength = fileBufferInputStream.read(fileBuffer)) >= 0) {
				signingSignature.update(fileBuffer, 0, fileBufferLength);
			};
			fileBufferInputStream.close();			
			byte[] realSignature = signingSignature.sign();

			//Write the signature to a text file
			FileOutputStream signatureFileOutputStream = new FileOutputStream(SIGNATURE_FILE_PATH);
			signatureFileOutputStream.write(realSignature);
			signatureFileOutputStream.close();
			
			//Write the public key to a file
			byte[] key = publicKey.getEncoded();
			FileOutputStream publicKeyFileOutputStream = new FileOutputStream(PUBLIC_KEY_FILE_PATH);
			publicKeyFileOutputStream.write(key);
			publicKeyFileOutputStream.close();
			
			System.out.println("The public key and the signature for the message have been generated.");			
		} catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException  | SignatureException | IOException e){
			System.err.println("Exception " + e.toString() + " caught");
		}
	}
}