package generate;

public class Parameters {

	public static final String SIGNING_AND_VERIFYING_ALGORITHM = "DSA";
	public static final String SIGNING_AND_VERIFYING_PROVIDER = "SUN";
	public static final String SECURE_RANDOM_ALGORITHM = "SHA1PRNG";
	public static final String SIGNATURE_ALGORITHM = "SHA1withDSA";
	
	public static final String PUBLIC_KEY_FILE_PATH = "files\\\\publicKey.txt";
	public static final String FALSE_PUBLIC_KEY_FILE_PATH = "files\\\\falsePublicKey.txt";
	public static final String SIGNATURE_FILE_PATH = "files\\\\signature.txt";
	public static final String DATA_FILE_PATH = "files\\\\text.txt";
	
}
