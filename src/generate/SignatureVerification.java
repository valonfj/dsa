package generate;

import java.io.*;
import java.security.*;
import java.security.spec.*;
import static generate.Parameters.*;

class SignatureVerification {

    public static void main(String[] args) {
        /* Verify the DSA signature */
        try {
        	//Read the public key
        	FileInputStream keyfile = new FileInputStream(PUBLIC_KEY_FILE_PATH);
        	byte[] encKey = new byte[keyfile.available()];  
        	keyfile.read(encKey);
        	keyfile.close();
        	
        	//Convert the public key
        	X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(encKey);        	
        	KeyFactory keyFactory = KeyFactory.getInstance(SIGNING_AND_VERIFYING_ALGORITHM, SIGNING_AND_VERIFYING_PROVIDER);
        	PublicKey publicKey = keyFactory.generatePublic(pubKeySpec);
        	
        	//Read the signature
			FileInputStream signatureFile = new FileInputStream(SIGNATURE_FILE_PATH);
			byte[] signatureToVerify = new byte[signatureFile.available()]; 
			signatureFile.read(signatureToVerify);
			signatureFile.close();
			
			//Initialize the signature object with the public key
			Signature verifyingSignature = Signature.getInstance(SIGNATURE_ALGORITHM, SIGNING_AND_VERIFYING_PROVIDER);			
			verifyingSignature.initVerify(publicKey);
			
			//Supply the signature object with the data to be verified
			FileInputStream dataFileInputStream = new FileInputStream(DATA_FILE_PATH);
			BufferedInputStream dataFileBufferInputStream = new BufferedInputStream(dataFileInputStream);

			byte[] dataFileBuffer = new byte[1024];
			int dataFileLength;
			while (dataFileBufferInputStream.available() != 0) {
				dataFileLength = dataFileBufferInputStream.read(dataFileBuffer);
			    verifyingSignature.update(dataFileBuffer, 0, dataFileLength);
			};
			dataFileBufferInputStream.close();
			//Verify
			boolean verified = verifyingSignature.verify(signatureToVerify);

			//Print the results
			if (verified) {
				System.out.println("The signature has been verified");
			}else {
				System.out.println("The signature cannot be trusted");
			}	
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException | IOException | InvalidKeyException | SignatureException e) {
            System.err.println("Caught exception " + e.toString());
        }
    }
}