package generate;

import java.io.FileOutputStream;
import java.security.*;

public class AdditionalKeyGenerator {

	public static void main(String[] args) {

		try{
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA", "SUN");		
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
			keyGen.initialize(1024, random);
			
			//Generate public key
			KeyPair pair = keyGen.generateKeyPair();
			PublicKey publicKey = pair.getPublic();
			
			byte[] key = publicKey.getEncoded();
			String publicKeyFile = "files\\falsePublicKey.txt";
			
			//Write key to a file
			FileOutputStream publicKeyFileOutputStream = new FileOutputStream(publicKeyFile);
			publicKeyFileOutputStream.write(key);
			publicKeyFileOutputStream.close();
			
			System.out.println("The public key is generated and has been written to the file.");
			
		} catch (Exception e){
			System.err.println("Exception " + e.toString() + " caught");
			
		}
	}

}
